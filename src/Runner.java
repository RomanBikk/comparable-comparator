import java.util.*;

public class Runner {

    public static void main(String[] args) {
        Comparator<User> comparator = Comparator.comparing(User -> User.getName().length());
        Set<User> users = new TreeSet<>(comparator);
        users.add(new User("Roman",13));
        users.add(new User("Bobiks",220));
        users.add(new User("Boolean",7));
        users.add(new User("Duck",3));
        users.add(new User("Deil",6));
        users.add(new User("Gek",66));
        for (User user : users) {
            System.out.println(user);

        }
    }
}
