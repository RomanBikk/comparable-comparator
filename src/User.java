public class User implements Comparable<User> {
    private String name;
    private int id;

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(User o) {
        return Integer.compare(this.id, o.id);


    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer("User's name ");
        buffer.append(this.name);
        buffer.append(" user's id ");
        buffer.append(this.id);
        return buffer.toString();
    }
}
